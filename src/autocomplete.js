let bookelems = document.getElementsByClassName("card-title");
let imgs = document.querySelectorAll(".card-image > img");

var data = {};
var books = [];

for (let i = 0; i < bookelems.length; i++) {
    let elem = bookelems[i].innerHTML;
    data[elem] = imgs[i].src;
    books.push(elem);
}

document.addEventListener('DOMContentLoaded', function() {
    M.Autocomplete.init( document.querySelectorAll('.autocomplete'), { data });
});

const sendItem = item => {
    if (books.includes(item)) window.location.href = `${rootPath}book/${item}.md`;
}