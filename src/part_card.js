const fs = require("fs");
const html = require('lit-ntml').htmlFragmentSync;
const parseMD = require('parse-md').default;
const config = require('../config.json');

module.exports = book => {
    const { metadata } = parseMD(fs.readFileSync(`${config.bookFolder}/${book.name}`, 'utf8'));

    return html`
        <div class="card hoverable" onclick="location.href = '${config.path}book/${book.name}'">
            <div class="card-image">
                <img src="${config.path}img/${metadata.image}">
            </div>
            <div class="card-content">
                <span class="card-title">${metadata.title}</span>
                <p>${metadata.author}</p>
            </div>
        </div>`.trim();
};