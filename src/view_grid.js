const fs = require("fs");
const genCard = require('./part_card');

module.exports = () => {
    let output = "<div class='grid'>";
    let book; // required as tmp variable (TODO: Get rid of)

    const dir = fs.opendirSync('books');
    while ((book = dir.readSync()) !== null) output += genCard(book);
    dir.closeSync()

    return output + "</div>";
};