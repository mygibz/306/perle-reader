const html = require('lit-ntml').htmlFragmentSync;

module.exports = html`
    <div class="input-field col s12">
        <i class="material-icons prefix">library_books</i>
        <input  type="text" id="autocomplete-input" class="autocomplete" onchange="sendItem(this.value)">
        <label for="autocomplete-input">Search</label>
    </div>`;