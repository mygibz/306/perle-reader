
const html = require('lit-ntml').htmlSync;
const config = require('../config.json');

module.exports = (content, title = config.applicationName, additionalContent = "") => {
    return html`<!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <meta name="description" content="A reader for all your book">
            <meta name="keywords" content="HTML, CSS, JavaScript, Material">
            <meta name="author" content="Yanik Ammann">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="shortcut icon" href="${config.path}logo" type="image/png">
            
            <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.css">
            <link rel="stylesheet" href="${config.path}style">
            
            <script defer src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
            <script defer src="${config.path}script"></script>

            <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

            <style id="customStyle"></style>
    
            <title>${title}</title>
        </head>
    
        <body>
            <nav class="indigo">
                <div class="nav-wrapper" style="margin-left: 15px">
                    <a href="${config.path}" class="brand-logo left" style="margin-left: 15px">
                        <i class="material-icons">chrome_reader_mode</i>
                        <span class="hide-on-small-only">${title}</span>
                    </a>
                    <div id="zoomButtons"></div>
                    <ul id="nav-mobile" class="right">
                        <li id="zoomIn"><a onclick="changeFontSize('+')">  <i class="material-icons">zoom_in</i> </a></li>
                        <li id="zoomOut"><a onclick="changeFontSize('-')">  <i class="material-icons">zoom_out</i> </a></li>
                        <li id="headerDropdownButton">
                            <a class='dropdown-trigger' href='#' data-target='headerDropdown'> <i class="material-icons">keyboard_arrow_down</i> </a>
                        </li>
                        <li><a onclick="logout()">  <i class="material-icons">logout</i> </a></li>
                    </ul>
                </div>
            </nav>
            <div style="height: 70px"></div>

            <!-- Dropdown Structure -->
            <!-- TODO: Remove -->
            <ul id='headerDropdown' class='dropdown-content'>
                <li><a href="#!">one</a></li>
            </ul>

            <div class="container">
                ${content}
                ${additionalContent}
            </div>
        </body>
    </html>`;
};