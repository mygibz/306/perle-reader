const fs = require("fs");
const html = require('lit-ntml').htmlFragmentSync;
const genCard = require('./part_card');

// configs
const countOfBooksToRecommend = 3;

module.exports = (count = 1) => {
    let books = [];
    let book; // required as tmp variable (TODO: Get rid of)
    const dir = fs.opendirSync('books');
    while ((book = dir.readSync()) !== null) books.push(book);
    dir.closeSync()

    // randomize order
    books = books.sort(() => Math.random() - 0.5);

    return html`
        <hr>
        <h2>You might also enjoy:</h2>
        <br>
        <div class="container grid">
            ${books.slice(0, countOfBooksToRecommend).map(genCard).join('')}
        </div>`;
};