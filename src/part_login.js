const html = require('lit-ntml').htmlFragmentSync;
const md5 = require('md5');

const correctLogins = require('../config.json').correctLogins;

// TODO: Compact
const checkLogin = (username, password) => {
	const loginsWithCorrectUsername = correctLogins.filter(login => login.username == username);

	if (loginsWithCorrectUsername.length < 1)
		return `No user found with username '${username}'!`;
	if (loginsWithCorrectUsername.length > 1)
		return `More than one username found with username '${username}'!`;

	if (loginsWithCorrectUsername[0].password == md5(password))
		return "yes";

	return `Wrong password used for user '${username}'!`;
}

const content = () => {
    return html`
	<style>.container.grid { display: unset; }</style>
	<div class="row" style="margin-top:15vh">
	<div class="col s3"></div>

		<form class="col s6 z-depth-1" style="padding:20px 30px 30px 30px" onsubmit="return false">
			<div class="row">
				<div class="input-field col s12">
					<input id="username" type="text" class="validate" />
					<label for="username">Username</label>
				</div>
				<div class="input-field col s12">
					<input id="password" type="password" class="validate" />
					<label for="password">Password</label>
				</div>
			</div>
			<div>
				<button class="btn waves-effect waves-light right indigo" onclick="checkLogin()">Login
					<i class="material-icons right">login</i>
				</button>
			</div>
		</form>

	</div>`;
};

module.exports = {
  checkLogin,
  content
}