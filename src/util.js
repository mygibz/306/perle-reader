let currentFontSize = 1;
const changeFontSize = operation => {
    const element = document.getElementById('customStyle');
    currentFontSize += (operation == '+') ? 0.1 : -0.1;
    element.innerHTML = `p {font-size: ${currentFontSize}em}`;
}

// Content title dropdown
document.addEventListener('DOMContentLoaded', () => {
    let headers = document.querySelectorAll(".container h1, .container h2, .container h3, .container h4, .container h5, .container h6");
    headers.forEach(element => element.id = encodeURI(element.innerText));
    document.getElementById('headerDropdown').innerHTML =
        Array.from(headers)
        .map(x => `<li><a href="#${x.id}">${x.innerText}</a></li>`)
        .join("\n");

    M.Dropdown.init(document.querySelectorAll('.dropdown-trigger'), {
        constrainWidth: false
    });
});

window.addEventListener("hashchange", () => {
    window.scrollTo(window.scrollX, window.scrollY - 100);
});

// Show Zoom buttons only when you are currently viewing a book
const path = new URL(document.URL).pathname;
const isBook = path.includes("book/");
document.getElementById('zoomIn').innerHTML = !isBook ? "" : `
    <li><a onclick="changeFontSize('+')">  <i class="material-icons">zoom_in</i> </a>`;
document.getElementById('zoomOut').innerHTML = !isBook ? "" : `
    <a onclick="changeFontSize('-')">  <i class="material-icons">zoom_out</i> </a></a>`;
document.getElementById('headerDropdownButton').style.display = !isBook ? "none" : "unset";


// TODO: Handle login (with params and so on)
const checkLogin = () => {
    fetch(`${rootPath}api/login`, {
        method: 'POST',

        body: JSON.stringify({
            username: document.querySelector('input#username').value,
            password: document.querySelector('input#password').value,
        }),

        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
    .then(resp => resp.json())
    .then(json => {
        if (json.success == "yes")
            window.location.href = rootPath;
        else
            M.toast({html: `Login failed: ${json.success}!`});
    })
    // TODO: Make catch work
    // .catch( M.toast({html: 'Something went wrong while trying to check the login!'}) );
}

const logout = () => {
    fetch(`${rootPath}api/logout`).then(() => {
        window.location.href = rootPath;
    })
}