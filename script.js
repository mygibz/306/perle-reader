const config = require('./config.json');

const fs = require("fs");
const http = require('http');
const sass = require('node-sass');
const Cookies = require('cookies');
const parseMD = require('parse-md').default;
const dateformat = require('dateformat');
const markdown = require( "markdown" ).markdown;

// Import HTML and CSS
console.log("Importing HTML...");
const login = require('./src/part_login');
const htmlRoot = require('./src/view_root');
const htmlAutocomplete = require('./src/part_autocomplete');
const htmlGrid = require('./src/view_grid');
const htmlPartRecommended = require('./src/part_recommended');

console.log("Importing SCSS...");
const style = sass.renderSync({
    file: './src/style.scss'
}).css;

console.log("Importing JS...");
const script =  `const rootPath = "${config.path}"\n` + 
                fs.readFileSync('./src/util.js') + 
                "\n" + 
                fs.readFileSync('./src/autocomplete.js')

// #region Custom Function to handle requests
const respond = (res, type, content, status = 200) => {
    res.writeHead(status, { 'Content-Type': type }); 
    res.write(content);
    res.end();
}

const handleImage = (res, path) => {
    try {
        respond(res, 'image/png', fs.readFileSync(`${config.coverFolder}/${path}`))
    } catch (err) {
        console.error(`${dateformat(config.timeFormatForLogging)}\tImage '${path}' not found`);
        respond(res, 'text/html', `Image '${path}' not Found`, 404);
    }
}

const handleBook = (res, path) => {
    try {
        const bookName = decodeURI(path);
        const content = parseMD(fs.readFileSync(`${config.bookFolder}/${bookName}`, 'utf8')).content;
        
        respond(res, 
            'text/html',
            htmlRoot(
                markdown.toHTML(content),
                bookName.replace('.md', ''),
                htmlPartRecommended()
            )
        );
    } catch (err) {
        try {
            const bookName = decodeURI(path);
            console.error(`${dateformat(config.timeFormatForLogging)}\tBook '${bookName}' not found`);
            respond(res, 'text/html', `Book '${bookName}' not found`, 404);
        } catch {
            console.error(`${dateformat(config.timeFormatForLogging)}\tBook '${path}' not found`);
            respond(res, 'text/html', `Book '${path}' not found`, 404);
        }
    }
}
// #endregion Custom Function to handle requests

// #region Webserver (hands off handling to functions)
var server = http.createServer((req, res) => {
    let cookies = new Cookies(req, res);

    console.log(`${dateformat(config.timeFormatForLogging)}\t${(cookies.get('loggedIn') == 'yes') ? "user" : "guest"}\tRequest: ${req.url}`);

    if (req.url == config.path)
        if (cookies.get('loggedIn') == 'yes')
            respond(res, 'text/html', htmlRoot(htmlAutocomplete + htmlGrid()))
        else
            respond(res, 'text/html', htmlRoot(login.content));

    //#region Login/logout

    // TODO: Make adapt to request (via post from client)
    else if (req.url == `${config.path}api/login`) {
        let body = '';
        req.on('data', chunk => { body += chunk.toString() });
        req.on('end', () => {
            body = JSON.parse(body);
            const loginSuccessful = login.checkLogin(body.username, body.password);
            if (loginSuccessful == "yes") cookies.set('loggedIn', 'yes')
            respond(res, 'text/json', JSON.stringify({ success: loginSuccessful }));
        });
    }

    else if (req.url == `${config.path}api/logout`) {
        cookies.set('loggedIn', 'no');
        res.end();
    }

    //#endregion
    
    else if (req.url == `${config.path}logo`)                respond(res, 'image/png', fs.readFileSync('logo.png'));
    else if (req.url == `${config.path}style`)               respond(res, 'text/css', style);
    else if (req.url == `${config.path}script`)              respond(res, 'application/javascript', script);
    else if (req.url.startsWith(`${config.path}img/`))       handleImage(res, req.url.split(`${config.path}img/`)[1]);
    else if (req.url.startsWith(`${config.path}book/`))    { handleBook(res, req.url.split(`${config.path}book/`)[1]) }
    else respond(res, 404, "Not Found");

});

server.listen(config.port, () => console.log(`Node.js web server running at http://localhost:${config.port}`));
// #endregion