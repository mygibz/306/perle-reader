# Perle Reader

A modern E-Book reader app, written in NodeJS.

## Getting Started
1. Go into folder
2. Install Dependencies
   - Either Automatic
     1. Run `npm install`
   - Or Manually
     1. Run `npm install cookies dateformat lit-ntml markdown md5 node-sass parse-md`
3. Either
    - (if opened in VS-Code) Press the run button in your IDE
    - Run `node script.js`

## Screenshots

![](.screenshot1.png)
![](.screenshot2.png)